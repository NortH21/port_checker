FROM python:3.7-alpine

EXPOSE 80

COPY . /app
WORKDIR /app

RUN pip3 install fastapi uvicorn

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
