# Port checker

Checking open ports using Python and interface on FasAPI

## Getting Started


### Prerequisites

- Git
- Python 3.6 or higher

### Installing


RedHat/CentOS/Fedora:

```
dnf install python3 python3-pip python3-devel
```

Install Python modules:
```
pip3 install fastapi uvicorn
```

Copy git repo:
```
git clone https://gitlab.com/NortH21/port_checker.git
```

## Running the tests

```
cd ./port_checker
uvicorn main:app --reload
```

And go to http://127.0.0.1:8000/docs


## Authors

* **Lebedev Yaroslav** - [NortH21](https://gitlab.com/NortH21)

See also the list of [contributors](https://gitlab.com/users/NortH21/projects) who participated in this project.
