import socket
from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI()

class Host(BaseModel):
    type: str = Field('tcp', example="tcp")
    host: str = Field(..., example="8.8.8.8")
    port: int = Field(..., example="80")
    password: str


@app.put("/{host}")
async def check_port(item: Host):
    if item.password == "HOyuYMTA":
        if item.type == "tcp":
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        elif item.type == "udp":
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        s.settimeout(5)
        result = s.connect_ex((item.host, item.port))

        if result == 0:
            result = "Open"
        elif result == 111:
            result = "Probably opened"
        elif result == 11:
            result = "Closed"
        else:
            result = "It is not clear whether it is open or not."
        return {"Status": result}
    else:
        return {"403": "Forbidden"}

